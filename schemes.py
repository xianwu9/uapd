import csv as csv
import numpy as np
from numpy import log,exp
from numpy.random import multinomial
import time
import datetime as DT
from matplotlib import pyplot as plt
from matplotlib.dates import date2num
import math
import collections
from scipy.spatial import distance
from matplotlib import cm
from scipy.stats import norm
import matplotlib.mlab as mlab
import scipy.optimize as opt
from scipy.stats import multivariate_normal
from sklearn import mixture
from scipy.interpolate import griddata
import operator
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
import pandas as pd
from scipy import  stats
import matplotlib.pyplot as plt
import statsmodels.api as sm
from statsmodels.graphics.api import qqplot
from statsmodels.tsa.ar_model import AR
from sktensor import dtensor, cp_als, ktensor, tucker
from statsmodels.tsa.vector_ar.var_model import VAR
from statsmodels.tsa.stattools import acf
from mpl_toolkits.mplot3d import Axes3D
from scipy.stats import uniform, gamma, poisson
import subprocess

start = time.time()
########################################################
#load files
def load_data(file_name):
    csv_file_name = csv.reader(open(file_name, 'rU'), dialect='excel') 
    header_file_name = csv_file_name.next()  
    column_file_name = {}
    for h in header_file_name:
        column_file_name[h] = []
    for row in csv_file_name:
        for h, v in zip(header_file_name, row):
            column_file_name[h].append(v)
    return header_file_name, column_file_name
####################
#load ID
def load_ID(file_name):
    csv_file_name = csv.reader(open(file_name, 'rU'), dialect='excel') 
    column_file_name = []
    for row in csv_file_name:
        for v in row:
            column_file_name.append(v)
    return column_file_name
####################
#load files
def load_data_txt(file_name):
    fdata = open(file_name, 'r')
    txt_file_name = [line.rstrip() for line in fdata]
    column_file_name = []
    for raw in txt_file_name:
        row = raw.strip('\r\n').split(' ')
        for v in row:
            column_file_name.append(v)
    return column_file_name
####################
#load files
def change_point(cp_list, num_list, len_list):
    np.random.seed(123456789)
    N = len_list; a = 2; b = 1; x = cp_list
    n=int(round(uniform.rvs()*N)); lambda1={}; lambda2={}
    for cp in cp_list:
        lambda1[cp] = gamma.rvs(a,scale=1./b)
        lambda2[cp] = gamma.rvs(a,scale=1./b)

    for e in range(700):
        for cp in cp_list:
            lambda1[cp]=gamma.rvs(a+sum(cp_list[cp][0:n]), scale=1./(n+b))
            lambda2[cp]=gamma.rvs(a+sum(cp_list[cp][n:N]), scale=1./(N-n+b))
        mult_n=np.array([0]*N)
        for i in range(N):
            tmp = 0
            for cp in cp_list:
                tmp = tmp + sum(cp_list[cp][0:i])*log(lambda1[cp])-i*lambda1[cp]+sum(cp_list[cp][i:N])*log(lambda2[cp])-(N-i)*lambda2[cp]
            mult_n[i] = tmp
        mult_n=exp(mult_n-max(mult_n))
        n=np.where(multinomial(1,mult_n/sum(mult_n),size=1)==1)[1][0]      
    return n

'*******************************************************************'
header_urban, column_urban = load_data('data1.csv')
set_region = [str(r) for r in sorted([int(i) for i in set(column_urban['Precinct'])])]

for index, i in enumerate(column_urban['outlier']):
    if "Noise" in i:
        column_urban['outlier'][index]="Noise"
set_outlier = set(column_urban['outlier'])


for i in header_urban:
    column_urban[i].reverse()
urban_date = ([date2num(DT.datetime.strptime( u, "%m/%d/%Y %H:%M")) for u in column_urban['date']])
urban_date_day = ([int(i) for i in urban_date])
sort_urban_date_day = list(sorted(set(urban_date_day)))
min_date_value = min(sort_urban_date_day)
date_list = []
for d in range(1,10):
    date_list.append('2014-12-0'+str(d))
print date_list
pred_day_list = [int(date2num(DT.datetime.strptime(d, "%Y-%m-%d"))) for d in date_list]
#####modified
sort_urban_date_week = [(date-(min_date_value-3))%7 for date in sort_urban_date_day]
sort_urban_week_weekends = []
for i in sort_urban_date_week:
    if i==6 or i==0:
        sort_urban_week_weekends.append(0)
    else:
        sort_urban_week_weekends.append(1)

##############################################

region_time = {}; time_region = {} 
for o in set_outlier:
    region_time[o] = {}; time_region[o] = {}
for r,o,d in zip(column_urban['Precinct'], column_urban['outlier'], urban_date_day):
    try:
        region_time[o][r].append(d)
    except KeyError:
        region_time[o][r] = [d]
    try:
        time_region[o][str(d)].append(r)
    except KeyError:
        time_region[o][str(d)] = [r]


ranks = range(5,10)
for rank in ranks:
    print 'rank:', rank
    TP_avg = {}; FP_avg = {}; TN_avg = {}; FN_avg = {}
    for o in set_outlier:
        TP_avg[o] = []; FP_avg[o] = []; TN_avg[o] = []; FN_avg[o] = []
    for pred_day in pred_day_list:
        print 'pred_day:', pred_day
        pred_dayth = pred_day-min_date_value; ground_truth_region = {}
        '*******************************************************************'
        for o in set_outlier:
            try:
                ground_truth_region[o] = time_region[o][str(pred_day)]
            except:
                ground_truth_region[o] = []
        ############################################################
        'generate_training_mtx'
        mtx_3D = []; column_outlier = {}
        for t in sort_urban_date_day:
            if int(t)<sort_urban_date_day[0]+pred_dayth:
                mtx_2D = []
                for r in set_region:
                    try:
                        mtx_1D = [region_time[o][r].count(t) for o in set_outlier]
                    except:
                        mtx_1D = [0 for o in range(len(set_outlier))]
                    mtx_2D.append(mtx_1D)
                mtx_3D.append(mtx_2D) 
                col_sum = np.sum(mtx_2D, axis=0)
                for idx,o in enumerate(set_outlier):
                    try:
                        column_outlier[o].append(col_sum[idx])
                    except KeyError:
                        column_outlier[o] = [col_sum[idx]]

        num_list = len(column_outlier[o])
        start_point = change_point(column_outlier, len(column_outlier), num_list)
        mtx_3D = mtx_3D[start_point:]
        T = dtensor(mtx_3D)
        P, fit, itr, exectimes = cp_als(T, rank, init='random')
        lmbda =  P.lmbda; sub_mtx_t = P.U[0]; sub_mtx_r = P.U[1]; sub_mtx_o = P.U[2]
        '***************************'
        try:
            var_mod = VAR(np.array(sub_mtx_t))
            var_mod.select_order(14)
            results = var_mod.fit(maxlags=14, ic='aic')
            lag_order = results.k_ar
            pred_t_one_day = results.forecast(sub_mtx_t[-lag_order:], 14)[0]
        except:
            try:
                var_mod = VAR(np.array(sub_mtx_t))
                var_mod.select_order(7)
                results = var_mod.fit(maxlags=7, ic='aic')
                lag_order = results.k_ar
                pred_t_one_day = results.forecast(sub_mtx_t[-lag_order:], 7)[0]
            except:
                var_mod = VAR(np.array(sub_mtx_t))
                var_mod.select_order(3)
                results = var_mod.fit(maxlags=3, ic='aic')
                lag_order = results.k_ar
                pred_t_one_day = results.forecast(sub_mtx_t[-lag_order:], 3)[0]

        '***************************************************************'
        th_r = 0.5; pred_mtx={}; tmp_max = 0; pred_region = {}
        '***************************************************************'
        pred_continue_value = {}
        for idx_r, r in enumerate(set_region):
            pred_mtx[r] = {}; pred_continue_value[r] = {}
            for idx_o,o in enumerate(set_outlier):
                pred_mtx[r][o] = sum([lmbda[rk]*sub_mtx_r[idx_r][rk]*sub_mtx_o[idx_o][rk]*pred_t_one_day[rk] for rk in range(rank)])
                pred_continue_value[r][o] = pred_mtx[r][o]
                if pred_mtx[r][o]>th_r:
                    try:
                        pred_region[o].append(r)
                    except KeyError:
                        pred_region[o] = [r]
                if pred_mtx[r][o]>tmp_max:
                    tmp_max = pred_mtx[r][o]

        for o in set_outlier:
            print o, 'positive regions:', pred_region[o]
            print '***'
            try:
                tmp_TP = [i for i in pred_region[o] if i in ground_truth_region[o]]
                TP = len(tmp_TP)
                FP_list = [i for i in pred_region[o] if i not in ground_truth_region[o]]
                FP = len(FP_list)
                TN = len(set([r for r in set_region if r not in pred_region[o]]).intersection(set([r for r in set_region if r not in ground_truth_region[o]])))
                FN_list = set([r for r in set_region if r not in pred_region[o]]).intersection(ground_truth_region[o])
                FN = len(FN_list)
            except KeyError:
                pass
            except ZeroDivisionError:
                pass
            TP_avg[o].append(TP);FP_avg[o].append(FP);TN_avg[o].append(TN);FN_avg[o].append(FN)
            print TP, FP, TN, FN
